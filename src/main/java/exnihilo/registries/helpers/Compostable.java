package exnihilo.registries.helpers;

import lombok.EqualsAndHashCode;
import net.minecraft.item.Item;

@EqualsAndHashCode
public class Compostable {

	public String unlocalizedName;
	public Item item;
	public int meta;
	public float value;
	public Color color;
	
	@Deprecated
	public Compostable(Item item, int meta, float value, Color color)
	{
		this.item = item;
		this.meta = meta;
		this.value = value;
		this.color = color;
	}
	
	public Compostable(float value, Color color)
	{
		this.value = value;
		this.color = color;
	}
}

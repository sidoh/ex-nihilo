package exnihilo.registries;

import java.util.Hashtable;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import exnihilo.registries.helpers.Meltable;

public class CrucibleRegistry {
	public static Hashtable<String, Meltable> entries = new Hashtable<String, Meltable>();
	
	/**
	 * Adds a block to the registry
	 * @param block Input block
	 * @param meta Input meta
	 * @param solidAmount Input amount, determines how much space in the crucible this takes, normally 2000 (or 5 blocks can fit)
	 * @param fluid Output fluid
	 * @param fluidAmount Output amount
	 * @param appearance Appearance in the crucible
	 * @param appearanceMeta Metadata to use for the appearance
	 */
	public static void register(Block block, int meta, float solidAmount, Fluid fluid, float fluidAmount, Block appearance, int appearanceMeta)
	{
		Meltable entry = new Meltable(block, meta, solidAmount, fluid, fluidAmount, appearance, appearanceMeta);
		entries.put(block + ":" + meta, entry);
	}
	
	/**
	 * Adds a block to the registry, automatically setting the display based on the input block and meta
	 * @param block Input block
	 * @param meta Input meta
	 * @param solidAmount Input amount, determines how much space in the crucible this takes, normally 2000 (or 5 blocks can fit)
	 * @param fluid Output fluid
	 * @param fluidAmount Output amount
	 */
	public static void register(Block block, int meta, float solidAmount, Fluid fluid, float fluidAmount)
	{
		register(block, meta, solidAmount, fluid, fluidAmount, block, meta);
	}
	
	/**
	 * Adds a block to the registry, setting 0 as the display meta
	 * This method mainly exists for backwards compatibility with mods using the original register method
	 * @param block Input block
	 * @param meta Input meta
	 * @param solidAmount Input amount, determines how much space in the crucible this takes, normally 2000 (or 5 blocks can fit)
	 * @param fluid Output fluid
	 * @param fluidAmount Output amount
	 * @param appearance Appearance in the crucible, using the icon for a meta of 0
	 */
	public static void register(Block block, int meta, float solidAmount, Fluid fluid, float fluidAmount, Block appearance)
	{
		Meltable entry = new Meltable(block, meta, solidAmount, fluid, fluidAmount, appearance, 0);
		entries.put(block + ":" + meta, entry);
	}
	/**
	 * Adds a block to the registry, along with all subblocks. Appearance will be set based on the specific input block
	 * @param block Input block
	 * @param solidAmount Input amount, determines how much space in the crucible this takes, normally 2000 (or 5 blocks can fit)
	 * @param fluid Output fluid
	 * @param fluidAmount Output amount
	 */
	public static void register(Block block, float solidAmount, Fluid fluid, float fluidAmount)
	{
		for (int i = 0; i < 16; i++)
			register(block, i, solidAmount, fluid, fluidAmount);
	}
	
	/**
	 * Checks if the registry contains a recipe
	 * @param block Input block
	 * @param meta Input meta
	 * @return True if the recipe is contained, false otherwise
	 */
	public static boolean containsItem(Block block, int meta)
	{
		return entries.containsKey(block + ":" + meta);
	}
	
	/**
	 * Gets the result of a crucible recipe
	 * @param block Input block
	 * @param meta Input meta
	 * @return A Meltable containing the result
	 */
	public static Meltable getItem(Block block, int meta)
	{
		return entries.get(block + ":" + meta);
	}
	
	public static void load(Configuration config)
	{
		//TODO Allow the ratios to be set in the config?
		//TODO Before that, make sure to handle the edge case where you get more fluid out than solids that you put in.
	}
	
	/**
	 * Changes the fluid that a block will turn into
	 * @param block Block to change registration of
	 * @param meta Meta of block to change registration of
	 * @param newFluid New fluid
	 * @param newVolume New fluid volume
	 * @return true if successful
	 */
	public static boolean changeFluidFromBlock(Block block, int meta, Fluid newFluid, float newVolume)
	{
		if (containsItem(block, meta))
		{
			Meltable melt = getItem(block, meta);
			melt.fluid = newFluid;
			melt.fluidVolume = newVolume;
			
			return true;
		}
		return false;
	}

	public static void registerMeltables() 
	{
		register(Blocks.cobblestone, 0, 2000, FluidRegistry.LAVA, 250);
		register(Blocks.stone, 0, 2000, FluidRegistry.LAVA, 250);
		register(Blocks.gravel, 0, 2000, FluidRegistry.LAVA, 250);
		register(Blocks.netherrack, 0, 2000, FluidRegistry.LAVA, 1000);
		
		register(Blocks.snow, 0, 2000, FluidRegistry.WATER, 500);
		register(Blocks.ice, 0, 2000, FluidRegistry.WATER, 1000);
	}
}

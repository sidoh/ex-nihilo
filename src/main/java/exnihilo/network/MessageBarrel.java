package exnihilo.network;

import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.simpleimpl.IMessage;

public class MessageBarrel implements IMessage {
	
	public int x, y, z;
	public int timer;
	
	public MessageBarrel() {}
	
	public MessageBarrel(int x, int y, int z, int timer)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.timer = timer;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) 
	{
		this.x = buf.readInt();
		this.y = buf.readInt();
		this.z = buf.readInt();
		this.timer = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) 
	{
		buf.writeInt(x);
		buf.writeInt(y);
		buf.writeInt(z);
		buf.writeInt(timer);	
	}

}

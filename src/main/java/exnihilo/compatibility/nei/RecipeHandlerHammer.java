package exnihilo.compatibility.nei;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.lwjgl.opengl.GL11;

import codechicken.lib.gui.GuiDraw;
import codechicken.nei.NEIServerUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler;
import exnihilo.registries.HammerRegistry;
import exnihilo.registries.helpers.Smashable;
import exnihilo.utils.ItemInfo;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

//I'll be honest, I've gotten most of this from Chisel 2.
public class RecipeHandlerHammer extends TemplateRecipeHandler {

	private static final int SLOTS_PER_PAGE = 9;

	public class CachedHammerRecipe extends CachedRecipe {

		private List<PositionedStack> input = new ArrayList<PositionedStack>();
		private List<PositionedStack> outputs = new ArrayList<PositionedStack>();
		public Point focus;

		public CachedHammerRecipe(List<ItemStack> variations, ItemStack base, ItemStack focus)
		{
			PositionedStack pstack = new PositionedStack(base != null ? base : variations, 74, 4);
			pstack.setMaxSize(1);
			this.input.add(pstack);

			int row = 0;
			int col = 0;
			for (ItemStack v : variations) 
			{
				this.outputs.add(new PositionedStack(v, 3 + 18 * col, 37 + 18 * row));

				if (focus != null && NEIServerUtils.areStacksSameTypeCrafting(focus, v)) {
					this.focus = new Point(2 + 18 * col, 36 + 18 * row);
				}

				col++;
				if (col > 8) {
					col = 0;
					row++;
				}
			}
		}

		public CachedHammerRecipe(List<ItemStack> variations) 
		{
			this(variations, null, null);
		}

		@Override
		public List<PositionedStack> getIngredients() 
		{
			return this.getCycledIngredients(cycleticks / 20, this.input);
		}

		@Override
		public List<PositionedStack> getOtherStacks() 
		{
			return this.outputs;
		}

		@Override
		public PositionedStack getResult() 
		{
			return null;
		}

	}

	@Override
	public String getRecipeName() 
	{
		return "Ex Nihilo Hammer";
	}

	@Override
	public String getGuiTexture() 
	{
		return "exnihilo:textures/hammerNEI.png"; 
	}

	private void addCached(List<ItemStack> variations, ItemStack base, ItemStack focus) 
	{
		if (variations.size() > SLOTS_PER_PAGE) 
		{
			List<List<ItemStack>> parts = new ArrayList<List<ItemStack>>();
			int size = variations.size();
			for (int i = 0; i < size; i += SLOTS_PER_PAGE) 
			{
				parts.add(new ArrayList<ItemStack>(variations.subList(i, Math.min(size, i + SLOTS_PER_PAGE))));
			}
			for (List<ItemStack> part : parts) 
			{
				this.arecipes.add(new CachedHammerRecipe(part, base, focus));
			}
		} 
		else 
		{
			this.arecipes.add(new CachedHammerRecipe(variations, base, focus));
		}
	}

	@Override
	public void drawBackground(int recipeIndex) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GuiDraw.changeTexture(getGuiTexture());
		GuiDraw.drawTexturedModalRect(0, 0, 0, 0, 166, 58);

		Point focus = ((CachedHammerRecipe) this.arecipes.get(recipeIndex)).focus;
		if (focus != null) 
		{
			GuiDraw.drawTexturedModalRect(focus.x, focus.y, 166, 0, 18, 18);
		}
	}

	@Override
	public int recipiesPerPage() {
		return 2;
	}

	@Override
	public void loadTransferRects() {
		this.transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(75, 22, 15, 13), "exnihilo.hammer", new Object[0]));
	}
	
	@Override
	public void loadCraftingRecipes(String outputID, Object... results)
	{
		if (outputID.equals("exnihilo.hammer"))
		{
			for (ItemInfo ii : HammerRegistry.getRewards().keySet())
			{
				ItemStack inputStack = ii.getStack();
				ArrayList<ItemStack> resultStack = new ArrayList<ItemStack>();
				HashMap<ItemInfo, Integer> cache = new HashMap<ItemInfo, Integer>();
				for (Smashable s : HammerRegistry.getRewards(ii))
				{
					ItemInfo currInfo = new ItemInfo(s.item, s.meta);
					if (cache.containsKey(currInfo))
						cache.put(currInfo, cache.get(currInfo)+1);
					else
						cache.put(currInfo,  1);
					
				}
				for (ItemInfo outputInfos : cache.keySet())
				{
					ItemStack stack = outputInfos.getStack();
					stack.stackSize = cache.get(outputInfos);
					resultStack.add(stack);
				}
				addCached(resultStack, inputStack, null);
			}
		}
		else
			super.loadCraftingRecipes(outputID, results);
	}

	@Override
	public void loadCraftingRecipes(ItemStack result) 
	{
		HashSet<ItemInfo> completed = new HashSet<ItemInfo>();
		for (ItemInfo ii : HammerRegistry.getSources(result))
		{
			if (!completed.contains(ii))
			{
				HashMap<ItemInfo, Integer> stored = new HashMap<ItemInfo, Integer>();
				for (Smashable results : HammerRegistry.getRewards(Block.getBlockFromItem(ii.getItem()), ii.getMeta()))
				{
					ItemInfo current = new ItemInfo(results.item, results.meta);
					if (stored.containsKey(current))
						stored.put(current, stored.get(current)+1);
					else
						stored.put(current, 1);
				}
				ArrayList<ItemStack> resultVars = new ArrayList<ItemStack>();
				for (ItemInfo info : stored.keySet())
				{
					ItemStack stack = info.getStack();
					stack.stackSize = stored.get(info);
					resultVars.add(stack);
				}
				addCached(resultVars, ii.getStack(), result);
				completed.add(ii);
			}
		}

	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		HashMap<ItemInfo, Integer> stored = new HashMap<ItemInfo, Integer>();
		
		if (Block.getBlockFromItem(ingredient.getItem()) == Blocks.air)
			return;
		
		if (!HammerRegistry.registered(ingredient))
			return;
		
		for (Smashable results : HammerRegistry.getRewards(Block.getBlockFromItem(ingredient.getItem()), ingredient.getItemDamage()))
		{
			ItemInfo current = new ItemInfo(results.item, results.meta);
			if (stored.containsKey(current))
				stored.put(current, stored.get(current)+1);
			else
				stored.put(current, 1);
		}
		ArrayList<ItemStack> resultVars = new ArrayList<ItemStack>();
		for (ItemInfo info : stored.keySet())
		{
			ItemStack stack = info.getStack();
			stack.stackSize = stored.get(info);
			resultVars.add(stack);
		}
		addCached(resultVars, ingredient, ingredient);
		//completed.add(ii);
	}

	@SuppressWarnings("unused")
	private void addCached(List<ItemStack> variations) 
	{
		addCached(variations, null, null);
	}

	@Override
	public List<String> handleItemTooltip(GuiRecipe guiRecipe, ItemStack itemStack, List<String> currenttip, int recipe) {
		super.handleItemTooltip(guiRecipe, itemStack, currenttip, recipe);
		CachedHammerRecipe crecipe = (CachedHammerRecipe) this.arecipes.get(recipe);
		Point mouse = GuiDraw.getMousePosition();
		Point offset = guiRecipe.getRecipePosition(recipe);
		Point relMouse = new Point(mouse.x - (guiRecipe.width - 176) / 2 - offset.x, mouse.y - (guiRecipe.height - 166) / 2 - offset.y);

		if (itemStack != null && (relMouse.y > 34 && relMouse.y < 55))
		{
			currenttip.add("Drop Chance:");
			ItemStack sourceStack = crecipe.input.get(0).item;
			Block inBlock = Block.getBlockFromItem(sourceStack.getItem());
			int meta = sourceStack.getItemDamage();
			for (Smashable smash : HammerRegistry.getRewards(inBlock, meta))
			{
				if (NEIServerUtils.areStacksSameTypeCrafting(itemStack, new ItemStack(smash.item, 1, smash.meta)))
				{
					int chance = (int) (100 * smash.chance);
					int fortune = (int) (100 * smash.luckMultiplier);
					if (fortune > 0)
						currenttip.add("  * "+Integer.toString(chance)+"%"
								+   EnumChatFormatting.BLUE + " (+"+Integer.toString(fortune)+"% luck bonus)"+EnumChatFormatting.RESET);
					else
						currenttip.add("  * "+Integer.toString(chance)+"%");

				}

			}

		}
		return currenttip;
	}



}
